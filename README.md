# WHMCS Tap2pay Payment Gateway Module

Supports WHMCS 7.2+.

## Install

- Upload files to WHMCS folder

- Create free account at http://tap2pay.me

- Go to [Settings](https://secure.tap2pay.me/merchants/settings) at Tap2pay account and copy API Key and Merchant Id 

- Select tab [Notifications](https://secure.tap2pay.me/merchants/settings/notifications) add Webhook URL `https://YOURDOMAIN.COM/modules/gateways/callback/tap2pay.php`
YOURDOMAIN.COM change, please, to your website domain name.  

- Add Tap2Pay in WHMCS dashboard and paste API Key and Merchant Id 

- For activating payments for production please contact a support or by email eugene at tap2pay dot me 