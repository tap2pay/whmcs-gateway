<?php
require_once "../../../init.php";

$whmcs->load_function("gateway");
$whmcs->load_function("invoice");
$GATEWAY = getGatewayVariables("tap2pay");
$gatewaymodule = "tap2pay";
if (!$GATEWAY["type"])
  die("Module Not Activated");

require_once(ROOTDIR . '/modules/gateways/tap2pay/helper.php');

$json = file_get_contents('php://input');
$obj = json_decode($json, true);

$custom = $obj['data']['custom'];
if ($obj['type'] == 'invoice.succeeded' && $custom) {
  if (preg_match('/^whmcs_(.+)/', $custom, $matches)){
    $id = checkCbInvoiceID($matches[1], $gatewaymodule);
    $transid = $obj['data']['id'];

    $command = 'GetTransactions';
    $postData = array(
      'transid' => $transid,
    );
    $results = localAPI($command, $postData);

    if($results['totalresults'] == 0) {
      $command = 'GetInvoice';
      $postData = array(
        'invoiceid' => $id,
      );
      $invoice = localAPI($command, $postData);
      if($invoice['status'] == 'Paid') {
        $rel = getRelByInvoiceId($id);
        $lastInvoiceId = getLastInvoiceIdForRel($rel['relid'], $rel['type']);

        $command = 'GetInvoice';
        $postData = array(
          'invoiceid' => $lastInvoiceId,
        );
        $lastInvoice = localAPI($command, $postData);
        if($lastInvoice['status'] != 'error') {
          $invoice = $lastInvoice;
        }
      }

      if($invoice['status'] == 'Unpaid') {
        $command = 'AddInvoicePayment';
        $postData = array(
          'invoiceid' => $invoice['invoiceid'],
          'transid' => $transid,
          'gateway' => $gatewaymodule,
          'date' => $obj['data']['created_at'],
        );

        $results = localAPI($command, $postData);
      } else {
        $command = 'AddCredit';
        $postData = array(
          'clientid' => $invoice['userid'],
          'description' => "Tap2pay invoice ".$transid,
          'amount' => $invoice['total'],
        );
        $results = localAPI($command, $postData);
      }
    }
  }
}
